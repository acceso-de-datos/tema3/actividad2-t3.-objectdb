package modelo;

public class Articulo {

	private String nombre;
	private float precio;
	private String codigo;
	private Grupo grupo;
	private int id;

	public Articulo() {

	}

	public Articulo(String nombre, float precio, String codigo, Grupo grupo) {

		this.nombre = nombre;
		this.precio = precio;
		this.codigo = codigo;
		this.grupo = grupo;

	}

	public Articulo(int id, String nombre, float precio, String codigo, Grupo grupo) {
		this.id = id;
		this.nombre = nombre;
		this.precio = precio;
		this.codigo = codigo;
		this.grupo = grupo;

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getPrecio() {
		return precio;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Grupo getGrupo() {
		return this.grupo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setGrupo(Grupo grupo) {
		this.grupo = grupo;
	}

	@Override
	public String toString() {
		return "Articulo{" +
				"nombre='" + nombre + '\'' +
				", precio=" + precio +
				", codigo='" + codigo + '\'' +
				", grupo=" + grupo +
				", id=" + id +
				'}';
	}
}
