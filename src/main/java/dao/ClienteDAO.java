package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import controladorbd.ConexionBD;
import modelo.Cliente;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * @author sergio
 */
public class ClienteDAO implements GenericDAO<Cliente> {
    static EntityManager con;


    public ClienteDAO() throws SQLException {
         con = controladorbd.ConexionBD.getConexion();

    }

    public void cerrar() throws SQLException {
        ConexionBD.cerrar();
    }

    public Cliente findByPK(int id)  {
    	
        Cliente c = null;
        TypedQuery<Cliente> query = con.createQuery("SELECT c FROM Cliente c WHERE id = ?1 ", Cliente.class);
        query.setParameter(1, id);
        c = query.getSingleResult();
        return c;
    }

    public List<Cliente> findAll()  {
        List<Cliente> listaClientes = new ArrayList<Cliente>();
        TypedQuery<Cliente> query = con.createQuery("SELECT c FROM Cliente c", Cliente.class);
        List<Cliente> results = query.getResultList();
        for (Cliente c : results) {
            listaClientes.add(new Cliente(c.getId(), c.getNombre(), c.getDireccion()));
        }

        return listaClientes;
    }


    public boolean insert(Cliente cliInsertar)  {
        Cliente cli = new Cliente(cliInsertar.getId(),cliInsertar.getNombre(),cliInsertar.getDireccion());
        con.getTransaction().begin();
        con.persist(cli);
        con.getTransaction().commit();

        return true;
    }


    public boolean update(Cliente cliActualizar)  {
      Cliente cliente = con.find(Cliente.class,cliActualizar.getId());

      cliente.setId(cliActualizar.getId());
      cliente.setNombre(cliActualizar.getNombre());
      cliente.setDireccion(cliActualizar.getDireccion());
        con.getTransaction().commit();
       return true;
    }

    public boolean delete(int id) {
        Cliente cliente = con.find(Cliente.class,id);
        con.getTransaction().begin();
        con.remove(cliente);
        con.getTransaction().commit();
        return true;
    }

    public boolean delete(Cliente cliEliminar){
        Cliente cliente = con.find(Cliente.class,cliEliminar.getId());
        con.getTransaction().begin();
        con.remove(cliente);
        con.getTransaction().commit();
        return true;

    }

}
