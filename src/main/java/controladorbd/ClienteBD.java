package controladorbd;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import dao.ClienteDAO;
import modelo.Cliente;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class ClienteBD {

	static EntityManager con;

	private ClienteDAO clieDao;
	private List<Cliente>listClientes;
	private int posicion;

	public ClienteBD() throws SQLException {
		con = ConexionBD.getConexion();
		TypedQuery<Cliente> query = con.createQuery("SELECT c FROM Cliente c", Cliente.class);

		clieDao = new ClienteDAO();
		listClientes = clieDao.findAll();
		posicion=0;
	}

	public Cliente actual() throws SQLException {
		return leer();
	}

	public Cliente primero() throws SQLException {
		if (totalRegistros() > 0) {
			rs.first();
			return leer();
		}
		return null;
	}

	public Cliente anterior() throws SQLException {
		if (totalRegistros() > 0) {
			rs.previous();
			if (rs.isBeforeFirst()) {
				rs.first();
			}
			return leer();
		}
		return null;
	}

	public Cliente siguiente() throws SQLException {
		if (totalRegistros() > 0) {
			rs.next();
			if (rs.isAfterLast()) {
				rs.last();
			}
			return leer();
		}
		return null;
	}

	public Cliente ultimo() throws SQLException {
		if (totalRegistros() > 0) {
			rs.last();
			return leer();
		}
		return null;
	}

	public Cliente ir(int r) throws SQLException {
		rs.absolute(r);
		return leer();
	}

	public boolean esPrimero() throws SQLException {
		return rs.isFirst();
	}

	public boolean esAntesPrimero() throws SQLException {
		return rs.isBeforeFirst();
	}

	public void irAntesPrimero() throws SQLException {
		rs.beforeFirst();
	}

	public boolean esUltimo() throws SQLException {
		return rs.isLast();
	}

	public void insertar(Cliente cli) throws SQLException {
		clieDao= new ClienteDAO();
		clieDao.insert(cli);
		
	}

	public void modificar(Cliente cli) throws SQLException {
		clieDao= new ClienteDAO();
		clieDao.update(cli);
		
	}
	 public Cliente findByPK(int id) throws SQLException {
		 clieDao = new ClienteDAO();
		 return clieDao.findByPK(id);
	    }

	private Cliente leer() throws SQLException {
		TypedQuery<Cliente> query = con.createQuery("SELECT c FROM Cliente c", Cliente.class);
		List<Cliente> results = query.getResultList();
		Cliente cliente = null;
		for (Cliente c : results) {
			System.out.println(c);
			new cliente(c.getId(), c.getNombre(), c.getDireccion());
		}
		return cliente;
		//return new Cliente(rs.getInt(1), rs.getString(2), rs.getString(3));
	}

	public void borrar() throws SQLException {
		rs.deleteRow();
	}

	public void cerrar() throws SQLException {
		stmt.close();
		rs.close();
	}

	public int registroActual() throws SQLException {
		return rs.getRow();
	}

	public int totalRegistros() throws SQLException {
		int bak = rs.getRow();
		rs.last();
		int num = rs.getRow();
		rs.absolute(bak);
		return num;
	}

	

}
